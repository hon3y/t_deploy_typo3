#!/bin/bash

##
## Variables
##
CONFIG_PATH="config/deploy"
RELEASES_PATH="releases"
RELEASES_DEVELOPMENT_PATH="${RELEASES_PATH}/development"
RELEASES_STAGING_PATH="${RELEASES_PATH}/staging"
RELEASES_PRODUCTION_PATH="${RELEASES_PATH}/production"
INSTALLER_PATH="t_installer"
TIME=$(date +"%Y-%m-%d_%H-%M-%S")
TRASH="trash/${TIME}"
#SQLDUMP_FOLDER="sqldump"
SQLDUMP_PATH="migrations/db"

##
## *.ini contains source-database credentials
##
SOURCE_INI=""

##
## *.ini to override in deployment
## containes target-database credentials
##
TARGET_INI=""

##
## Generated file
##
GENERATED_FILE=""

##
## ignore tables
##
IGNORE_TABLES=""

update () {
    case "$1" in
        "development")
            case "$2" in
                "staging")

                ##
                ## there must be exactly one staging version
                ##
                COUNT_DIRECTORIES_IN_GIVEN_PATH=$(find ${RELEASES_STAGING_PATH} -mindepth 1 -maxdepth 1 -type d | wc -l)
                if [ ${COUNT_DIRECTORIES_IN_GIVEN_PATH} = 1 ];
                    then
                        ##
                        ## version must be equal to $3
                        ##
                        FIRST_DIRECTORY_IN_GIVEN_PATH=$(find ${RELEASES_STAGING_PATH} -type d | sort | tail -n +2 | head -1 | sed 's#.*/##')
                        if [ ! "${FIRST_DIRECTORY_IN_GIVEN_PATH}" = "${3}" ];
                            then
                                errorMsg "First directory in '${RELEASES_STAGING_PATH}' is '${FIRST_DIRECTORY_IN_GIVEN_PATH}'.\n" \
                                "${RELEASES_STAGING_PATH}/${LAST_DIRECTORY_IN_GIVEN_PATH} != ${RELEASES_STAGING_PATH}/${3}"
                                exitMsg;
                                exit;
                        fi
                    else
                        errorMsg "Count directories in '${RELEASES_STAGING_PATH}' = ${COUNT_DIRECTORIES_IN_GIVEN_PATH} != 1."
                        exitMsg;
                        exit;
                fi

                ##
                ## check wether master branch exists in local INSTALLER
                ##
                BRANCH=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH branch --list "${3}")
                if [ "$BRANCH" = "" ]
                    then
                        checkNotMsg "Target branch '${3}' not merged and checked out in local INSTALLER."
                        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                        "Please fetch && merge && checkout branch '${3}' in local INSTALLER first. \n" \
                        "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                        exitMsg
                        exit
                fi

                ##
                ## composer update and install-extension
                ##

                #include deploy.ini variables
                runMsg ". t_deploy/deploy.ini"
                . t_deploy/deploy.ini

                logMsg "${t_deploy_typo3__phpbinary} composer.phar update"
                "${t_deploy_typo3__phpbinary}" ${RELEASES_STAGING_PATH}/${3}/app/composer.phar update
                sleep 2

                logMsg "\"${t_deploy_typo3__phpbinary}\" ${RELEASES_STAGING_PATH}/${3}/install_extensions.sh"
                "${t_deploy_typo3__phpbinary}" ${RELEASES_STAGING_PATH}/${3}/app/install_extensions.sh
                sleep 2

                ##
                ## set symlink
                ##
                logMsg "update symlink for staging"

                logMsg "move current staging symlink to trash"
                mv -v staging ${TRASH}
                sleep 1

                logMsg "set staging symlink"
                ln -s ${RELEASES_STAGING_PATH}/${3}/app/web/ staging

                ;;
                *)
                errorMsg "Wrong argument\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        "staging")
            case "$2" in
                "production")

                ##
                ## highest version must be equal to $3
                ##
                LAST_DIRECTORY_IN_GIVEN_PATH=$(find ${RELEASES_PRODUCTION_PATH} -type d | sort --reverse | head -1 | sed 's#.*/##')
                if [ ! "${LAST_DIRECTORY_IN_GIVEN_PATH}" = "${3}" ]; then
                    errorMsg "Last directory in '${RELEASES_PRODUCTION_PATH}' is '${LAST_DIRECTORY_IN_GIVEN_PATH}'.\n" \
                    "${RELEASES_PRODUCTION_PATH}/${LAST_DIRECTORY_IN_GIVEN_PATH} != ${RELEASES_PRODUCTION_PATH}/${3}"
                    exitMsg;
                    exit;
                fi

                ##
                ## t_installer/.git must be present
                ##
                if [ ! -d ${INSTALLER_PATH}/.git ]; then
                    errorMsg "Directory '${INSTALLER_PATH}/.git' does not exist\n" \
                    "Feel free to read the manual by executing 'make help'"
                    exitMsg
                    exit
                fi

                ##
                ## check wether master branch exists in local INSTALLER
                ##
                BRANCH=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH branch --list "master")
                if [ "$BRANCH" = "" ]
                    then
                        checkNotMsg "Target branch 'master' does not exist in local INSTALLER."
                        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                        "Please fetch && merge && checkout target branch 'master' in local INSTALLER first. \n" \
                        "Branch from current development branch. \n" \
                        "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                        exitMsg
                        exit
                    else
                    ##
                    ## check wether tag ${3} exists in local INSTALLER
                    ##
                    TAG=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH tag --list "${3}")
                    if [ "$TAG" = "" ]
                    then
                        checkNotMsg "Tag '${3}' does not exist in 'master' in local INSTALLER."
                        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                        "Please fetch && merge && checkout tag '${3}' in local INSTALLER first. \n" \
                        "Do _NOT_ just create tag '${3}' in local INSTALLER. \n" \
                        "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                        exitMsg
                        exit
                    fi
                fi

                ##
                ## composer update and install-extension
                ##

                #include deploy.ini variables
                runMsg ". t_deploy/deploy.ini"
                . t_deploy/deploy.ini

                logMsg "\"${t_deploy_typo3__phpbinary}\" ${RELEASES_PRODUCTION_PATH}/${3}/app/composer.phar update"
                "${t_deploy_typo3__phpbinary}" ${RELEASES_PRODUCTION_PATH}/${3}/app/composer.phar update
                sleep 2

                logMsg "\"${t_deploy_typo3__phpbinary}\" ${RELEASES_PRODUCTION_PATH}/${3}/install_extensions.sh"
                "${t_deploy_typo3__phpbinary}" ${RELEASES_PRODUCTION_PATH}/${3}/app/install_extensions.sh
                sleep 2

                ##
                ## rename databases
                ##
                logMsg "renaming production databases"
                mv -v config/deploy/production.ini config/deploy/production_temp.ini
                sleep 1
                mv -v config/deploy/production1.ini config/deploy/production.ini
                sleep 1
                mv -v config/deploy/production_temp.ini config/deploy/production1.ini
                sleep 1

                logMsg "move current production symlink to trash"
                mv -v production ${trash}
                sleep 1

                ##
                ## set symlink
                ##
                logMsg "set new symlink for production"
                ln -s ${RELEASES_PRODUCTION_PATH}/$3/app/web/ production
                sleep 1

                ;;
                *)
                errorMsg "Wrong argument\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        "production")
            case "$2" in
                "development")

                ##
                ## highest version must be equal to $3
                ##
                LAST_DIRECTORY_IN_GIVEN_PATH=$(find ${RELEASES_PRODUCTION_PATH} -type d | sort --reverse | head -1 | sed 's#.*/##')
                if [ ! "${LAST_DIRECTORY_IN_GIVEN_PATH}" = "${3}" ]; then
                    errorMsg "Last directory in '${RELEASES_PRODUCTION_PATH}' is '${LAST_DIRECTORY_IN_GIVEN_PATH}'.\n" \
                    "${RELEASES_PRODUCTION_PATH}/${LAST_DIRECTORY_IN_GIVEN_PATH} != ${RELEASES_PRODUCTION_PATH}/${3}"
                    exitMsg;
                    exit;
                fi

                ##
                ## t_installer/.git must be present
                ##
                if [ ! -d ${INSTALLER_PATH}/.git ]; then
                    errorMsg "Directory '${INSTALLER_PATH}/.git' does not exist\n" \
                    "Feel free to read the manual by executing 'make help'"
                    exitMsg
                    exit
                fi

                ##
                ## check wether master branch exists in local INSTALLER
                ##
                BRANCH=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH branch --list "master")
                if [ "$BRANCH" = "" ]
                    then
                        checkNotMsg "Target branch 'master' does not exist in local INSTALLER."
                        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                        "Please fetch && merge && checkout target branch 'master' in local INSTALLER first. \n" \
                        "Branch from current development branch. \n" \
                        "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                        exitMsg
                        exit
                    else
                    ##
                    ## check wether tag ${3} exists in local INSTALLER
                    ##
                    TAG=$(git --git-dir=$INSTALLER_PATH/.git --work-tree=$INSTALLER_PATH tag --list "${3}")
                    if [ "$TAG" = "" ]
                    then
                        checkNotMsg "Tag '${3}' does not exist in 'master' in local INSTALLER."
                        errorMsg "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n" \
                        "Please fetch && merge && checkout tag '${3}' in local INSTALLER first. \n" \
                        "Do _NOT_ just create tag '${3}' in local INSTALLER. \n" \
                        "+ - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -\n"
                        exitMsg
                        exit
                    fi
                fi

                ##
                ## composer update and install-extension
                ##

                #include deploy.ini variables
                runMsg ". t_deploy/deploy.ini"
                . t_deploy/deploy.ini

                logMsg "${t_deploy_typo3__phpbinary} composer.phar update"
                "${t_deploy_typo3__phpbinary}" ${RELEASES_DEVELOPMENT_PATH}/development/app/composer.phar update
                sleep 2

                logMsg "\"${t_deploy_typo3__phpbinary}\" ${RELEASES_STAGING_PATH}/development/install_extensions.sh"
                "${t_deploy_typo3__phpbinary}" ${RELEASES_DEVELOPMENT_PATH}/development/app/install_extensions.sh
                sleep 2

                ;;

                *)
                errorMsg "Wrong argument\n" \
                "'$1' => '$2' not valid\n" \
                "Feel free to read the manual by executing 'make help'"
                exitMsg
                exit
                ;;
            esac
        ;;

        ###################################
        ## Default
        ###################################
        *)
        errorMsg "Wrong argument [TODO]\n" \
        "Feel free to read the manual by executing 'make help'"
        exitMsg
        exit
        ;;
    esac


}

ignoreTables() {
    . t_deploy/db-helper.ini

    IGNORE_TABLES=""
    for TABLE in "${production_ignore_tables[@]}"
    do :
        IGNORE_TABLES+=" --ignore-table=${SOURCE_DATABASE}.${TABLE}"
    done
}

errorMsg() {
    echo -e "" \
    "$(tput setaf 1)$(tput setab 0)"
    warningMsg "[${BASH_LINENO}]"
    echo -e $(tput sgr 0) $(tput setaf 1)$(tput setab 0)$*$(tput sgr 0)
}

checkMsg() {
    echo -e "" \
    "$(tput setaf 2)[x] $*$(tput sgr 0)"
}
checkWarningMsg() {
    echo -e "" \
    "$(tput setaf 3)[!] $*$(tput sgr 0)"
}
checkNotMsg() {
    echo -e $(tput setaf 1)$(tput setab 0)"" \
    "$(tput setaf 1)$(tput setab 0)[-] $*$(tput sgr 0)"
}

runMsg() {
    echo -e "" \
    "[run] '$*' [$(tput setaf 3)$(tput setab 0)${BASH_LINENO}$(tput sgr 0)]"
}

logMsg() {
    echo -e "" \
    "$*"
}

infoMsg() {
    echo -e "" \
    "\n" \
    "$*"
}

exitMsg() {
    errorMsg "[EXIT]\n" \
    "Good by'"

    cd t_deploy
}

warningMsg() {
    echo -e "" \
    "$(tput setaf 3)$(tput setab 0)$*$(tput sgr 0)"
}

infoMsg "Hello!"

if [ "$#" -lt 1 ] || [ "$#" -lt 2 ] || [ "$#" -lt 3 ]; then
    errorMsg "Missing argument\n" \
    "Feel free to read the manual by executing 'make help'"
    exit
fi

SOURCE_INI="${1}.ini"
##
## production.ini is in use already, so we have to use our production1.ini temporary.
## we mv production.ini and production1.ini later (but not in this script)!
##
if [ "${2}" = "production" ]; then TARGET_INI="${2}1.ini"; else TARGET_INI="${2}.ini"; fi
GENERATED_FILE="${1}__${2}__${3}__${TIME}.sh"

##
## important
##
cd ..

##
## run :)
## e.g. development staging 0.1
## e.g. staging production 0.1
## e.g. production development development
##
update $1 $2 $3